﻿using System;
using System.Collections.Generic;
using System.Linq;
using Machine.Specifications.Annotations;
using Training.DomainClasses;

namespace Training.Spec
{
    public class Where<Titem> 
    {
        public static CriteriaBuilder<Titem, Tproperty> hasA<Tproperty>(Func<Titem, Tproperty> getProperty) 
        {
            return new CriteriaBuilder<Titem, Tproperty>(getProperty);
        }
    }

    public class CriteriaBuilder<Titem, Tproperty> 
    {
        internal Func<Titem, Tproperty> getProperty;

        public CriteriaBuilder(Func<Titem, Tproperty> getProperty)
        {
            this.getProperty = getProperty;
        }
    }

    public static class CriteriaBuilderExtensions
    {
        public static Criteria<Titem> equalTo<Titem, Tproperty>(this CriteriaBuilder<Titem, Tproperty> @this, Tproperty other)
        {
            return new AnonymousCriteria<Titem>(item => @this.getProperty(item).Equals(other));
        }

        public static Criteria<Titem> equalToAny<Titem, Tproperty>(this CriteriaBuilder<Titem, Tproperty> @this, params Tproperty[] otherItems)
        {
            var acceptableItems = new List<Tproperty>(otherItems);
            return new AnonymousCriteria<Titem>(item => acceptableItems.Contains(@this.getProperty(item)));
        }

        public static Criteria<Titem> greaterThan<Titem, Tproperty>(this CriteriaBuilder<Titem, Tproperty> @this, Tproperty value) where Tproperty : IComparable<Tproperty>
        {
            return new AnonymousCriteria<Titem>(item => value.CompareTo(@this.getProperty(item)) < 0);
        }
    }
}