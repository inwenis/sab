﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Training.DomainClasses;

namespace Training.DomainClasses
{
    public static class CriteriaExtensions
    {
        public static Criteria<Pet> Or(this Criteria<Pet> first, Criteria<Pet> second)
        {
            return new Alternative<Pet>(first, second);
        }

        public static Criteria<Pet> And(this Criteria<Pet> first, Criteria<Pet> second)
        {
            return new Conjunction<Pet>(first, second);
        }
    }
}
