using System;
using Training.DomainClasses;

public interface Criteria<Titem>
{
    bool IsSatisfiedBy(Titem item);
}