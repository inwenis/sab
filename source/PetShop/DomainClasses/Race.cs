﻿using System;

namespace Training.DomainClasses
{
    public class Race
    {
        public static readonly Race Rabbit = new Race(3, EnviromentType.Cage);
        public static readonly Race Mouse = new Race(1, EnviromentType.Cage);
        public static readonly Race Cat = new Race(7, EnviromentType.Home);
        public static readonly Race Snake = new Race(1, EnviromentType.Terrarium);
        public static readonly Race Dog = new Race(12, EnviromentType.Home);
        public static readonly Race Giraffe = new Race(10, EnviromentType.Home);

        public readonly int estimatedLengtOfLife;
        public readonly EnviromentType environment;

        public Race(int estimatedLengtOfLife, EnviromentType environment)
        {
            this.estimatedLengtOfLife = estimatedLengtOfLife;
            this.environment = environment;
        }
    }
}
