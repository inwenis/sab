using System;
using System.Collections.Generic;

namespace Training.DomainClasses
{
    public class Pet : IEquatable<Pet>
    {
        public bool Equals(Pet other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return String.Equals(name, other.name);
        }

             public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Pet) obj);
        }

        public override int GetHashCode()
        {
            return (name != null ? name.GetHashCode() : 0);
        }

        public static bool operator ==(Pet left, Pet right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Pet left, Pet right)
        {
            return !Equals(left, right);
        }

        public Sex sex;
        public string name { get; set; }
        public int yearOfBirth { get; set; }
        public float price { get; set; }
        public Race race { get; set; }

        public static Criteria<Pet> IsCatOrDog()
        {
            return new IsSpeciesCriteria(Race.Cat, Race.Dog);
        }

        public static Predicate<Pet> IsNotAMouse()
        {
            return pet => pet.race != Race.Mouse;
        }

        public static Criteria<Pet> IsSpecies(Race race)
        {
            return new IsSpeciesCriteria(race);
        }

        public static Criteria<Pet> IsBornAfter(int year)
        {
            return new IsBornAfterCriteria(year);
        }

        public static Predicate<Pet> IsDogAndBornAfter2010()
        {
            return pet => pet.yearOfBirth >= 2011 && pet.race == Race.Dog;
        }

        public static Predicate<Pet> IsRabbitOrBornAfter2010()
        {
            return pet => pet.yearOfBirth > 2011 || pet.race == Race.Rabbit;
        }

        public static Predicate<Pet> IsMaleDog()
        {
            return pet=>pet.sex == Sex.Male && pet.race == Race.Dog;
        }

        public static Predicate<Pet> IsRabbitOrIsBOrnAfter2010()
        {
            return pet=>pet.yearOfBirth > 2011 || pet.race == Race.Rabbit;
        }

        public static Predicate<Pet> IsSpecies(params Race[] cat)
        {
            var acceptableSpecies = new List<Race>(cat);
            return pet => acceptableSpecies.IndexOf(pet.race) >= 0;
        }

        public class IsBornAfterCriteria : Criteria<Pet>
        {
            private readonly int _year;

            public IsBornAfterCriteria(int year)
            {
                _year = year;
            }

            public bool IsSatisfiedBy(Pet item)
            {
                return item.yearOfBirth > _year;
            }
        }

        public class IsFemaleCriteria : Criteria<Pet>
        {
            public bool IsSatisfiedBy(Pet item)
            {
                return item.sex == Sex.Female;
            }
        }

        public class IsSpeciesCriteria : Criteria<Pet>
        {
            private readonly Race[] _races;

            public IsSpeciesCriteria(params Race[] races)
            {
                _races = races;
            }

            public bool IsSatisfiedBy(Pet item)
            {
                return new List<Race>(_races).Contains(item.race);
            }
        }

        public static Criteria<Pet> IsFemale()
        {
            return new IsFemaleCriteria();
        }
    }
}