using System;
using System.Collections.Generic;

namespace Training.DomainClasses
{
    public class PetShop
    {
        private IList<Pet> _petsInTheStore;

        public PetShop(IList<Pet> petsInTheStore)
        {
            this._petsInTheStore = petsInTheStore;
        }

        public IEnumerable<Pet> AllPets()
        {
            return new ReadOnlySet<Pet>(_petsInTheStore);
        }

        public void Add(Pet newPet)
        {
            if (_petsInTheStore.Contains(newPet))
            {
                return;
            }

            _petsInTheStore.Add(newPet);
        }

        
        public IEnumerable<Pet> AllPetsSortedByName()
        {
            var sortedList = new List<Pet>(_petsInTheStore);
            sortedList.Sort((p1,p2)=>p1.name.CompareTo(p2.name));
            return sortedList;
        }

        public IEnumerable<Pet> AllCats()
        {
            return _petsInTheStore.AllItemsThat(Pet.IsSpecies(Race.Cat));
        }

        public IEnumerable<Pet> AllMices()
        {
            return _petsInTheStore.AllItemsThat(Pet.IsSpecies(Race.Mouse));
        }

        public IEnumerable<Pet> AllFemalePets()
        {
            return _petsInTheStore.AllItemsThat(Pet.IsFemale());
        }

        public IEnumerable<Pet> AllCatsOrDogs()
        {
            return _petsInTheStore.AllItemsThat(Pet.IsSpecies(Race.Cat,Race.Dog));
        }

        public IEnumerable<Pet> AllPetsButNotMices()
        {
            return _petsInTheStore.AllItemsThat(new Negation<Pet>(Pet.IsSpecies(Race.Mouse)));
        }

        public IEnumerable<Pet> AllPetsBornAfter2010()
        {
            return _petsInTheStore.AllItemsThat(Pet.IsBornAfter(2010));
        }

        public IEnumerable<Pet> AllDogsNotOlderThan3Years()
        {
            return _petsInTheStore.AllItemsThat(Pet.IsSpecies(Race.Dog).And(Pet.IsBornAfter(2010)));
        }

        public IEnumerable<Pet> AllMaleDogs()
        {
            return _petsInTheStore.AllItemsThat(Pet.IsMaleDog());
        }

        public IEnumerable<Pet> AllPetsBornAfter2011OrRabbits()
        {
            return _petsInTheStore.AllItemsThat(Pet.IsSpecies(Race.Rabbit).Or(Pet.IsBornAfter(2011)));
        }
    }

    public abstract class BinaryCriteria<TItem> : Criteria<TItem>
    {
        protected Criteria<TItem> _firstCriteria;
        protected Criteria<TItem> _secondCriteria;

        public BinaryCriteria(Criteria<TItem> firstCriteria, Criteria<TItem> secondCriteria)
        {
            _firstCriteria = firstCriteria;
            _secondCriteria = secondCriteria;
        }

        public abstract bool IsSatisfiedBy(TItem item);
    }

    public class Alternative<T> : BinaryCriteria<Pet>
    {
        public Alternative(Criteria<Pet> firstCriteria, Criteria<Pet> secondCriteria) : base(firstCriteria, secondCriteria)
        {
        }

        public override bool IsSatisfiedBy(Pet item)
        {
            return _firstCriteria.IsSatisfiedBy(item) || _secondCriteria.IsSatisfiedBy(item);
        }
    }

    public class Conjunction<T> : BinaryCriteria<Pet>
    {

        public Conjunction(Criteria<Pet> firstCriteria, Criteria<Pet> secondCriteria) : base(firstCriteria,secondCriteria)
        {
        }

        public override bool IsSatisfiedBy(Pet item)
        {
            return _firstCriteria.IsSatisfiedBy(item) && _secondCriteria.IsSatisfiedBy(item);
        }
    }
}